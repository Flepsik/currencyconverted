package flepsik.github.com.currencyconverter.screens.main.impl;

import java.util.List;

import flepsik.github.com.currencyconverter.common.LoadingCallback;
import flepsik.github.com.currencyconverter.model.Currency;
import flepsik.github.com.currencyconverter.repository.database.CurrencyRepository;
import flepsik.github.com.currencyconverter.repository.network.NetworkRepository;
import flepsik.github.com.currencyconverter.screens.main.ConverterModel;

public class DefaultConverterModel implements ConverterModel {
    private CurrencyRepository databaseRepository;
    private NetworkRepository networkRepository;

    public DefaultConverterModel(CurrencyRepository databaseRepository, NetworkRepository networkRepository) {
        this.databaseRepository = databaseRepository;
        this.networkRepository = networkRepository;
    }

    @Override
    public void getCurrencies(final LoadingCallback<List<Currency>> callback) {
        networkRepository.loadCurrencies(new LoadingCallback<List<Currency>>() {
            @Override
            public void onSuccess(List<Currency> items) {
                databaseRepository.deleteAll();
                databaseRepository.insertOrReplace(items);
                callback.onSuccess(items);
            }

            @Override
            public void onError(Exception e) {
                databaseRepository.findAll(callback);
            }
        });
    }
}
