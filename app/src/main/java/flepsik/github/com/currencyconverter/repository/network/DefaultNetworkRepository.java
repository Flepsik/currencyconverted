package flepsik.github.com.currencyconverter.repository.network;

import android.os.AsyncTask;
import android.text.TextUtils;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.ParseException;
import java.util.List;

import flepsik.github.com.currencyconverter.common.LoadingCallback;
import flepsik.github.com.currencyconverter.common.utils.CurrencyUtils;
import flepsik.github.com.currencyconverter.model.Currency;

public class DefaultNetworkRepository implements NetworkRepository {
    private static final String URL = "http://www.cbr.ru/scripts/XML_daily.asp";

    @Override
    public void loadCurrencies(LoadingCallback<List<Currency>> callback) {
        new FetchFeedTask(URL, callback).execute();
    }

    private class FetchFeedTask extends AsyncTask<Void, Void, List<Currency>> {

        private String urlLink;
        private LoadingCallback<List<Currency>> callback;

        public FetchFeedTask(String urlLink, LoadingCallback<List<Currency>> callback) {
            this.urlLink = urlLink;
            this.callback = callback;
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected List<Currency> doInBackground(Void... voids) {
            List<Currency> result = null;
            if (!TextUtils.isEmpty(urlLink)) {
                try {
                    if (!urlLink.startsWith("http://") && !urlLink.startsWith("https://")) {
                        urlLink = "http://" + urlLink;
                    }

                    URL url = new URL(urlLink);
                    InputStream inputStream = url.openConnection().getInputStream();
                    result = CurrencyUtils.parseFeed(inputStream);
                } catch (IOException | XmlPullParserException | ParseException e) {
                    e.printStackTrace();
                }
            }
            return result;
        }

        @Override
        protected void onPostExecute(List<Currency> success) {
            if (success != null) {
                callback.onSuccess(success);
            } else {
                callback.onError(new Exception());
            }
        }
    }
}
