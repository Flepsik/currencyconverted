package flepsik.github.com.currencyconverter.screens.main.impl;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Patterns;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import flepsik.github.com.currencyconverter.R;
import flepsik.github.com.currencyconverter.model.Currency;

public final class CurrencyFragmentDialog extends DialogFragment {
    public static final String TAG = "CurrencyFragmentDialog";
    public static final String KEY_CURRENCIES = "CURRENCIES";
    public static final String KEY_SELECTED = "SELECTED";
    public static final String KEY_DIALOG_TYPE = "DIALOG_TYPE";

    public static CurrencyFragmentDialog newInstance(int dialogType, List<Currency> currencies, int selected) {
        Bundle args = new Bundle();
        args.putParcelableArrayList(KEY_CURRENCIES, new ArrayList<>(currencies));
        args.putInt(KEY_SELECTED, selected);
        args.putInt(KEY_DIALOG_TYPE, dialogType);
        CurrencyFragmentDialog fragment = new CurrencyFragmentDialog();
        fragment.setArguments(args);
        return fragment;
    }

    private CurrencyDialogListener listener;
    private CurrencyFragmentAdapter adapter;
    private List<Currency> currencies;
    private int selectedItem;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        currencies = getArguments().getParcelableArrayList(KEY_CURRENCIES);
        if (savedInstanceState != null) {
            selectedItem = savedInstanceState.getInt(KEY_SELECTED);
        } else {
            selectedItem = getArguments().getInt(KEY_SELECTED);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = getActivity().getLayoutInflater().inflate(R.layout.recycler, null);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyler);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getContext(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        selectedItem = position;
                        adapter.setSelectedIndex(selectedItem);
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                        selectedItem = position;
                        adapter.setSelectedIndex(selectedItem);
                    }
                }));
        adapter = new CurrencyFragmentAdapter(currencies, selectedItem);
        recyclerView.setAdapter(adapter);
        builder.setView(recyclerView)
                .setTitle(R.string.choose)
                .setPositiveButton(R.string.choose, (null))
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        CurrencyFragmentDialog.this.closeDialog();
                    }
                });
        return builder.create();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (CurrencyDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement CurrencyDialogListener");
        }
    }

    // Prevent dialog from closing on Add clicked
    @Override
    public void onResume() {
        super.onResume();
        final AlertDialog dialog = (AlertDialog) getDialog();
        if (dialog != null) {
            Button positiveButton = dialog.getButton(Dialog.BUTTON_POSITIVE);
            positiveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onPositiveButtonClicked();
                }
            });
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_SELECTED, selectedItem);
    }

    private void onPositiveButtonClicked() {
        Currency currency = currencies.get(selectedItem);
        if (listener != null) {
            listener.onCurrencyChoosen(getArguments().getInt(KEY_DIALOG_TYPE), currency);
            closeDialog();
        }
    }

    private void closeDialog() {
        getDialog().cancel();
    }

    interface CurrencyDialogListener {
        void onCurrencyChoosen(int dialogType, Currency currency);
    }

    public static class RecyclerItemClickListener implements RecyclerView.OnItemTouchListener {
        private OnItemClickListener mListener;

        public static abstract class OnItemClickListener {
            public void onItemClick(View view, int position) {
            }

            public void onLongItemClick(View view, int position) {
            }
        }

        GestureDetector mGestureDetector;

        public RecyclerItemClickListener(Context context, final RecyclerView recyclerView, OnItemClickListener listener) {
            mListener = listener;
            mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && mListener != null) {
                        mListener.onLongItemClick(child, recyclerView.getChildAdapterPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView view, MotionEvent e) {
            View childView = view.findChildViewUnder(e.getX(), e.getY());
            if (childView != null && mListener != null && mGestureDetector.onTouchEvent(e)) {
                mListener.onItemClick(childView, view.getChildAdapterPosition(childView));
                return true;
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView view, MotionEvent motionEvent) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
        }
    }
}