package flepsik.github.com.currencyconverter.screens.main.impl;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import flepsik.github.com.currencyconverter.R;
import flepsik.github.com.currencyconverter.model.Currency;

public class CurrencyFragmentAdapter extends RecyclerView.Adapter<CurrencyFragmentAdapter.ViewHolder> {
    private List<Currency> items = new ArrayList<>();
    private int selectedIndex = 0;

    public CurrencyFragmentAdapter(List<Currency> items, int selectedIndex) {
        this.items = items;
        this.selectedIndex = selectedIndex;
    }

    public int getSelectedIndex() {
        return selectedIndex;
    }

    public void setSelectedIndex(int selectedIndex) {
        this.selectedIndex = selectedIndex;
        notifyDataSetChanged();
    }

    @Override
    public CurrencyFragmentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_radio_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CurrencyFragmentAdapter.ViewHolder holder, int position) {
        holder.bind(items.get(position), position == selectedIndex);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        RadioButton radioButton;

        ViewHolder(View itemView) {
            super(itemView);
            radioButton = (RadioButton) itemView;
        }

        void bind(Currency currency, boolean isSelected) {
            radioButton.setText(currency.getCode() + " (" + currency.getName() + ")");
            radioButton.setChecked(isSelected);
        }
    }
}