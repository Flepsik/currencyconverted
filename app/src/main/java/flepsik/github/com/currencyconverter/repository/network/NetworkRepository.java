package flepsik.github.com.currencyconverter.repository.network;

import java.util.List;

import flepsik.github.com.currencyconverter.common.LoadingCallback;
import flepsik.github.com.currencyconverter.model.Currency;

public interface NetworkRepository {
    void loadCurrencies(LoadingCallback<List<Currency>> callback);
}
