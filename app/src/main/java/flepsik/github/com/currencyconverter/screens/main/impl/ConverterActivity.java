package flepsik.github.com.currencyconverter.screens.main.impl;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v4.widget.PopupMenuCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;
import java.util.List;

import flepsik.github.com.currencyconverter.R;
import flepsik.github.com.currencyconverter.common.PresenterActivity;
import flepsik.github.com.currencyconverter.common.PresenterFactory;
import flepsik.github.com.currencyconverter.model.Currency;
import flepsik.github.com.currencyconverter.repository.database.impl.DefaultCurrencyRepository;
import flepsik.github.com.currencyconverter.repository.network.DefaultNetworkRepository;
import flepsik.github.com.currencyconverter.screens.main.ConverterModel;
import flepsik.github.com.currencyconverter.screens.main.ConverterPresenter;
import flepsik.github.com.currencyconverter.screens.main.ConverterView;

public class ConverterActivity extends PresenterActivity<ConverterView, ConverterPresenter>
        implements ConverterView,
        View.OnClickListener,
        CurrencyFragmentDialog.CurrencyDialogListener {

    private static final int DIALOG_SOURCE = 1;
    private static final int DIALOG_TARGET = 2;

    private SwipeRefreshLayout swipeRefreshLayout;
    private EditText sourceValueEdit;
    private Button sourceCurrencyButton;
    private TextView targetValueView;
    private Button targetCurrencyButton;
    private Button convertButton;
    private View mainInfo;
    private ProgressBar progressBar;
    private View placeHolder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        sourceValueEdit = (EditText) findViewById(R.id.source_value_text);
        sourceCurrencyButton = (Button) findViewById(R.id.source_currency_button);
        targetValueView = (TextView) findViewById(R.id.target_value_text);
        targetCurrencyButton = (Button) findViewById(R.id.target_currency_button);
        convertButton = (Button) findViewById(R.id.convert_button);
        progressBar = (ProgressBar) findViewById(R.id.progress);
        placeHolder = findViewById(R.id.placeholder);
        mainInfo = findViewById(R.id.main_info);

        sourceValueEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                presenter.onSourceValueChanged(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.onRefreshRequested();
            }
        });
        sourceCurrencyButton.setOnClickListener(this);
        targetCurrencyButton.setOnClickListener(this);
        convertButton.setOnClickListener(this);
    }

    @NonNull
    @Override
    protected PresenterFactory<ConverterView, ConverterPresenter> getPresenterFactory() {
        return new PresenterFactory<ConverterView, ConverterPresenter>() {
            @Override
            public ConverterPresenter create() {
                return new DefaultConverterPresenter(getModel());
            }
        };
    }

    @NonNull
    @Override
    protected ConverterView getBindView() {
        return this;
    }

    protected ConverterModel getModel() {
        return new DefaultConverterModel(new DefaultCurrencyRepository(this), new DefaultNetworkRepository());
    }

    @Override
    public void showProgress(boolean shouldShow) {
        progressBar.setVisibility(shouldShow ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showRefreshing(boolean shouldShow) {
        swipeRefreshLayout.setRefreshing(shouldShow);
    }

    @Override
    public void showPlaceholder(boolean shouldShow) {
        placeHolder.setVisibility(shouldShow ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showMainInfo(boolean shouldShow) {
        mainInfo.setVisibility(shouldShow ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setSourceCurrency(Currency currency) {
        sourceCurrencyButton.setText(currency.getCode()
                        + (currency.getNominal() > 1 ? "(" + currency.getNominal() + ")" : "")
        );
    }

    @Override
    public void setTargetCurrency(Currency currency) {
        targetCurrencyButton.setText(
                currency.getCode()
                        + (currency.getNominal() > 1 ? "(" + currency.getNominal() + ")" : "")
        );
    }

    @Override
    public void setSourceValue(String value) {
        sourceValueEdit.setText(value);
    }

    @Override
    public void showResult(String value) {
        targetValueView.setText(value);
    }

    @Override
    public void showChooseSourceDialog(List<Currency> currencies, int selectedIndex) {
        CurrencyFragmentDialog.newInstance(DIALOG_SOURCE, currencies, selectedIndex)
                .show(getSupportFragmentManager(), CurrencyFragmentDialog.TAG);
    }

    @Override
    public void showChooseTargetDialog(List<Currency> currencies, int selectedIndex) {
        CurrencyFragmentDialog.newInstance(DIALOG_TARGET, currencies, selectedIndex)
                .show(getSupportFragmentManager(), CurrencyFragmentDialog.TAG);
    }

    @Override
    public void showErrorMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.target_currency_button:
                presenter.onTargetCurrencyClicked();
                break;
            case R.id.convert_button:
                presenter.onCalculateClicked();
                break;
            case R.id.source_currency_button:
                presenter.onSourceCurrencyClicked();
                break;
        }
    }

    @Override
    public void onCurrencyChoosen(int dialogType, Currency currency) {
        if (dialogType == DIALOG_TARGET) {
            presenter.onTargetCurrencyChanged(currency);
        } else if (dialogType == DIALOG_SOURCE) {
            presenter.onSourceCurrencyChanged(currency);
        }
    }
}
