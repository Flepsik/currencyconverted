package flepsik.github.com.currencyconverter.common;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;


public abstract class PresenterActivity<View extends BindView, P extends Presenter<View>> extends AppCompatActivity {
    protected P presenter;
    protected String tag;
    private PresenterDelegator<View> presenterDelegator;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tag = loadTag(savedInstanceState);
        setupPresenter();
        presenterDelegator = new PresenterDelegator<>(presenter);
        presenterDelegator.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenterDelegator.bindView(getBindView());
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenterDelegator.unbindView();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        presenterDelegator.saveState(outState);
        saveTag(outState);
    }

    protected String loadTag(Bundle savedInstance) {
        String tag = PresenterUtils.loadTag(savedInstance);
        if (TextUtils.isEmpty(tag)) {
            tag = generateTag();
        }

        return tag;
    }

    protected void saveTag(Bundle outState) {
        PresenterUtils.saveTag(outState, tag);
    }

    protected String generateTag() {
        return PresenterUtils.generateUniqueTag();
    }

    protected void setupPresenter() {
        presenter = PresenterCache.getInstance().getPresenter(tag,
                getPresenterFactory(),
                PresenterCache.CacheType.TEMPORARY);
    }

    @NonNull
    protected abstract PresenterFactory<View, P> getPresenterFactory();

    @NonNull
    protected abstract View getBindView();
}

