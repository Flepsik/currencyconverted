package flepsik.github.com.currencyconverter.screens.main;

import java.util.List;

import flepsik.github.com.currencyconverter.common.BindView;
import flepsik.github.com.currencyconverter.model.Currency;

public interface ConverterView extends BindView {
    void showProgress(boolean shouldShow);
    void showRefreshing(boolean shouldShow);
    void showPlaceholder(boolean shouldShow);
    void showMainInfo(boolean shouldShow);
    void setSourceCurrency(Currency currency);
    void setTargetCurrency(Currency currency);
    void setSourceValue(String value);
    void showResult(String value);
    void showChooseSourceDialog(List<Currency> currencies, int checkedIndex);
    void showChooseTargetDialog(List<Currency> currencies, int checkedIndex);
    void showErrorMessage(String message);
}
