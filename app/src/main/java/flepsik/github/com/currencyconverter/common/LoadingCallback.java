package flepsik.github.com.currencyconverter.common;

public interface LoadingCallback<Item> {
    void onSuccess(Item item);

    void onError(Exception e);
}
