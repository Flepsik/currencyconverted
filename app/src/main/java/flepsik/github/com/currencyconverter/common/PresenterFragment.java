package flepsik.github.com.currencyconverter.common;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;

public abstract class PresenterFragment<P extends Presenter<View>, View extends BindView> extends Fragment {
    protected P presenter;
    protected String tag;
    private PresenterDelegator<View> presenterDelegator;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tag = loadTag(savedInstanceState);
        setupPresenter();
        presenterDelegator = new PresenterDelegator<>(presenter);
        presenterDelegator.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        presenterDelegator.bindView(getBindView());
    }

    @Override
    public void onStop() {
        super.onStop();
        presenterDelegator.unbindView();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        presenterDelegator.saveState(outState);
        saveTag(outState);
    }

    protected String loadTag(Bundle savedInstance) {
        String tag = PresenterUtils.loadTag(savedInstance);
        if (TextUtils.isEmpty(tag)) {
            tag = generateTag();
        }

        return tag;
    }

    protected void saveTag(Bundle outState) {
        PresenterUtils.saveTag(outState, tag);
    }

    protected String generateTag() {
        return PresenterUtils.generateUniqueTag();
    }

    protected void setupPresenter() {
        presenter = PresenterCache.getInstance().getPresenter(tag,
                getPresenterFactory(),
                PresenterCache.CacheType.TEMPORARY);
    }

    @NonNull
    protected abstract PresenterFactory<View, P> getPresenterFactory();

    @NonNull
    protected abstract View getBindView();
}
