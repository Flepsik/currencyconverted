package flepsik.github.com.currencyconverter.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Currency implements Parcelable {
    private long id;
    private String code;
    private String name;
    private int nominal;
    private double value;

    public Currency(long id, String code, String name, int nominal, double value) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.nominal = nominal;
        this.value = value;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getNominal() {
        return nominal;
    }

    public void setNominal(int nominal) {
        this.nominal = nominal;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.code);
        dest.writeInt(this.nominal);
        dest.writeString(this.name);
        dest.writeDouble(this.value);
    }

    protected Currency(Parcel in) {
        this.id = in.readLong();
        this.code = in.readString();
        this.nominal = in.readInt();
        this.name = in.readString();
        this.value = in.readDouble();
    }

    public static final Parcelable.Creator<Currency> CREATOR = new Parcelable.Creator<Currency>() {
        @Override
        public Currency createFromParcel(Parcel source) {
            return new Currency(source);
        }

        @Override
        public Currency[] newArray(int size) {
            return new Currency[size];
        }
    };
}
