package flepsik.github.com.currencyconverter.screens.main.impl;

import android.text.TextUtils;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import flepsik.github.com.currencyconverter.common.BasePresenter;
import flepsik.github.com.currencyconverter.common.LoadingCallback;
import flepsik.github.com.currencyconverter.model.Currency;
import flepsik.github.com.currencyconverter.screens.main.ConverterModel;
import flepsik.github.com.currencyconverter.screens.main.ConverterPresenter;
import flepsik.github.com.currencyconverter.screens.main.ConverterView;

public class DefaultConverterPresenter extends BasePresenter<ConverterView> implements ConverterPresenter {
    private final ConverterModel model;
    private List<Currency> currencyList;
    private Currency currentCurrency;
    private Currency targetCurrency;
    private double currentSourceValue = 1;
    private double currentResultValue = 1;

    @LoadingState
    int state = STATUS_NONE;

    public DefaultConverterPresenter(ConverterModel model) {
        this.model = model;
        initialize();
    }

    @Override
    public void updateView() {
        ConverterView view = view();
        if (view != null) {
            view.showProgress(state == STATUS_LOADING);
            view.showRefreshing(state == STATUS_REFRESHING);
            view.showPlaceholder(state == STATUS_LOADED_ALL && currencyList.size() == 0);
            view.showMainInfo(currencyList.size() > 0);
            if (currencyList.size() > 0) {
                view.setSourceCurrency(currentCurrency);
                view.setTargetCurrency(targetCurrency);
                view.setSourceValue(NumberFormat.getInstance().format(currentSourceValue));
                view.showResult(NumberFormat.getInstance().format(currentResultValue));
            }
        }
    }

    @Override
    public void onRefreshRequested() {
        if (state != STATUS_LOADING) {
            state = STATUS_REFRESHING;
            model.getCurrencies(new CurrencyLoadingCallback());
        }
        ConverterView view = view();
        if (view != null) {
            view.showRefreshing(state == STATUS_REFRESHING);
        }
    }

    @Override
    public void onSourceValueChanged(String value) {
        try {
            currentSourceValue = NumberFormat.getInstance().parse(value).doubleValue();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCalculateClicked() {
        currentResultValue = calculateResult();
        ConverterView view = view();
        if (view != null) {
            view.showResult(NumberFormat.getInstance().format(currentResultValue));
        }
    }

    @Override
    public void onSourceCurrencyClicked() {
        ConverterView view = view();
        if (view != null) {
            view.showChooseSourceDialog(currencyList, currencyList.indexOf(currentCurrency));
        }
    }

    @Override
    public void onTargetCurrencyClicked() {
        ConverterView view = view();
        if (view != null) {
            view.showChooseTargetDialog(currencyList, currencyList.indexOf(targetCurrency));
        }
    }

    @Override
    public void onSourceCurrencyChanged(Currency currency) {
        currentCurrency = currency;
        currentResultValue = calculateResult();
        updateView();
    }

    @Override
    public void onTargetCurrencyChanged(Currency currency) {
        targetCurrency = currency;
        currentResultValue = calculateResult();
        updateView();
    }

    private double calculateResult() {
        return currentSourceValue * currentCurrency.getValue() / targetCurrency.getValue() * targetCurrency.getNominal() / currentCurrency.getNominal();
    }

    private void onError() {
        ConverterView view = view();
        if (view != null) {
            view.showErrorMessage("Something wrong has happened while loading currencies :c");
        }
    }

    private void initialize() {
        currencyList = new ArrayList<>();
        state = STATUS_LOADING;
        model.getCurrencies(new CurrencyLoadingCallback());
    }

    private class CurrencyLoadingCallback implements LoadingCallback<List<Currency>> {
        @Override
        public void onSuccess(List<Currency> items) {
            state = STATUS_LOADED_ALL;
            DefaultConverterPresenter.this.currencyList = items;
            if (currencyList.size() > 0) {
                DefaultConverterPresenter.this.currentCurrency = getCurrency(currentCurrency);
                DefaultConverterPresenter.this.targetCurrency = getCurrency(targetCurrency);
                currentResultValue = calculateResult();
            }
            updateView();
        }

        @Override
        public void onError(Exception e) {
            state = STATUS_READY;
            updateView();
            DefaultConverterPresenter.this.onError();
        }

        private Currency getCurrency(Currency lastCurrency) {
            if (lastCurrency == null) {
                if (currencyList.size() > 0) {
                    return currencyList.get(0);
                }
            }

            if (lastCurrency != null) {
                for (Currency newCurrency : currencyList) {
                    if (lastCurrency.getId() == newCurrency.getId()) {
                        return newCurrency;
                    }
                }
            }
            return null;
        }
    }
}
