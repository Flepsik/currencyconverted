package flepsik.github.com.currencyconverter.screens.main;

import flepsik.github.com.currencyconverter.common.Presenter;
import flepsik.github.com.currencyconverter.model.Currency;

public interface ConverterPresenter extends Presenter<ConverterView> {
    void onRefreshRequested();
    void onSourceValueChanged(String value);
    void onCalculateClicked();
    void onSourceCurrencyClicked();
    void onTargetCurrencyClicked();
    void onSourceCurrencyChanged(Currency currency);
    void onTargetCurrencyChanged(Currency currency);
}
