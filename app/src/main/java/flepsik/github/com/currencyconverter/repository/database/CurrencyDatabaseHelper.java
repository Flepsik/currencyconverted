package flepsik.github.com.currencyconverter.repository.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class CurrencyDatabaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "CurrencyDatabase";
    private static final int DATABASE_VERSION = 1;

    public static final String TABLE_NAME = "currency";
    public static final String KEY_ID = "_id";
    public static final String KEY_CODE = "code";
    public static final String KEY_NAME = "name";
    public static final String KEY_NOMINAL = "nominal";
    public static final String KEY_VALUE = "value";
    public static final String[] DEFAULT_PROJECTION = new String[]{
            KEY_ID,
            KEY_CODE,
            KEY_NAME,
            KEY_NOMINAL,
            KEY_VALUE
    };
    public static final String DEFAULT_SORT_ORDER = KEY_ID + " ASC";
    private static final String CREATE_TABLE =
            "create table " + TABLE_NAME + "("
                    + KEY_ID + " integer primary key not null, "
                    + KEY_CODE + " string not null, "
                    + KEY_NAME + " string not null, "
                    + KEY_NOMINAL + " integer not null, "
                    + KEY_VALUE + " real not null);";


    public CurrencyDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(sqLiteDatabase);
    }
}
