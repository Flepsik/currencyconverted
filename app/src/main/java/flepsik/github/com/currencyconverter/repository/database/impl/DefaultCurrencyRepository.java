package flepsik.github.com.currencyconverter.repository.database.impl;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import flepsik.github.com.currencyconverter.common.LoadingCallback;
import flepsik.github.com.currencyconverter.common.utils.CurrencyUtils;
import flepsik.github.com.currencyconverter.model.Currency;
import flepsik.github.com.currencyconverter.repository.database.CurrencyDatabaseHelper;
import flepsik.github.com.currencyconverter.repository.database.CurrencyRepository;

public class DefaultCurrencyRepository implements CurrencyRepository {
    private CurrencyDatabaseHelper databaseHelper;

    public DefaultCurrencyRepository(Context context) {
        databaseHelper = new CurrencyDatabaseHelper(context);
    }

    @Override
    public void insertOrReplace(final Currency currency) {
        if (CurrencyUtils.isValidToSave(currency)) {
            new Thread() {
                @Override
                public void run() {
                    synchronized (currency) {
                        SQLiteDatabase database = databaseHelper.getWritableDatabase();
                        database.insertWithOnConflict(
                                CurrencyDatabaseHelper.TABLE_NAME,
                                null,
                                CurrencyUtils.toContentValues(currency),
                                SQLiteDatabase.CONFLICT_REPLACE
                        );
                    }
                }
            }.run();
        }
    }

    @Override
    public void insertOrReplace(final List<Currency> currencies) {
        if (currencies != null && currencies.size() > 0) {
            new Thread() {
                @Override
                public void run() {
                    SQLiteDatabase database = databaseHelper.getWritableDatabase();
                    for (Currency currency : currencies) {
                        long id = database.insertWithOnConflict(
                                CurrencyDatabaseHelper.TABLE_NAME,
                                null,
                                CurrencyUtils.toContentValues(currency),
                                SQLiteDatabase.CONFLICT_REPLACE
                        );
                        Log.d("inserted", "Id = " + id);
                    }
                }
            }.run();
        }
    }

    @Override
    public void delete(final long id) {
        if (id > 0) {
            new Thread() {
                @Override
                public void run() {
                    SQLiteDatabase database = databaseHelper.getWritableDatabase();
                    String where = CurrencyDatabaseHelper.KEY_ID + " = " + id;
                    database.delete(
                            CurrencyDatabaseHelper.TABLE_NAME,
                            where,
                            null
                    );
                }
            }.run();
        }
    }

    @Override
    public void deleteAll() {
        new Thread() {
            @Override
            public void run() {
                SQLiteDatabase database = databaseHelper.getWritableDatabase();
                database.delete(
                        CurrencyDatabaseHelper.TABLE_NAME,
                        null,
                        null
                );
            }
        }.run();
    }

    private static final int STATE_SUCCESS = 0;
    private static final int STATE_FAIL = 1;

    @SuppressLint("HandlerLeak")
    @Override
    public void findAll(final LoadingCallback<List<Currency>> callback) {
        final List<Currency> result = new CopyOnWriteArrayList<>();
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case STATE_SUCCESS:
                        callback.onSuccess(result);
                        break;
                    case STATE_FAIL:
                        callback.onError(new Exception("Failed to download from database"));
                        break;
                    default:
                        super.handleMessage(msg);
                }
            }
        };

        new Thread() {
            @Override
            public void run() {
                SQLiteDatabase database = databaseHelper.getReadableDatabase();
                Cursor cursor = database.query(CurrencyDatabaseHelper.TABLE_NAME,
                        CurrencyDatabaseHelper.DEFAULT_PROJECTION,
                        null, null, null, null,
                        CurrencyDatabaseHelper.DEFAULT_SORT_ORDER
                );
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        do {
                            long id = cursor.getLong(cursor.getColumnIndex(CurrencyDatabaseHelper.KEY_ID));
                            String code = cursor.getString(cursor.getColumnIndex(CurrencyDatabaseHelper.KEY_CODE));
                            String name = cursor.getString(cursor.getColumnIndex(CurrencyDatabaseHelper.KEY_NAME));
                            int nominal = cursor.getInt(cursor.getColumnIndex(CurrencyDatabaseHelper.KEY_NOMINAL));
                            double value = cursor.getDouble(cursor.getColumnIndex(CurrencyDatabaseHelper.KEY_VALUE));
                            result.add(new Currency(id, code, name, nominal, value));
                        } while (cursor.moveToNext());
                    }
                    cursor.close();
                    handler.sendEmptyMessage(STATE_SUCCESS);
                } else {
                    handler.sendEmptyMessage(STATE_FAIL);
                }
            }
        }.run();
    }

    @SuppressLint("HandlerLeak")
    @Override
    public void find(final int index, final LoadingCallback<Currency> callback) {
        final Currency[] currency = {null};
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case STATE_SUCCESS:
                        callback.onSuccess(currency[0]);
                        break;
                    case STATE_FAIL:
                        callback.onError(new Exception("Failed to load currency with id = " + index + "from database"));
                        break;
                    default:
                        super.handleMessage(msg);
                }
            }
        };

        new Thread() {
            @Override
            public void run() {
                SQLiteDatabase database = databaseHelper.getReadableDatabase();
                String where = CurrencyDatabaseHelper.KEY_ID + " = " + index;
                Cursor cursor = database.query(CurrencyDatabaseHelper.TABLE_NAME,
                        CurrencyDatabaseHelper.DEFAULT_PROJECTION,
                        where,
                        null, null, null,
                        CurrencyDatabaseHelper.DEFAULT_SORT_ORDER
                );
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        long id = cursor.getLong(cursor.getColumnIndex(CurrencyDatabaseHelper.KEY_ID));
                        String code = cursor.getString(cursor.getColumnIndex(CurrencyDatabaseHelper.KEY_CODE));
                        String name = cursor.getString(cursor.getColumnIndex(CurrencyDatabaseHelper.KEY_NAME));
                        int nominal = cursor.getInt(cursor.getColumnIndex(CurrencyDatabaseHelper.KEY_NOMINAL));
                        double value = cursor.getDouble(cursor.getColumnIndex(CurrencyDatabaseHelper.KEY_VALUE));
                        currency[0] = new Currency(id, code, name, nominal, value);
                    }
                    cursor.close();
                }
                handler.sendEmptyMessage(currency[0] != null ? STATE_SUCCESS : STATE_FAIL);
            }
        }.run();
    }
}
