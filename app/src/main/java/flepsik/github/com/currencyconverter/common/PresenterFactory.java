package flepsik.github.com.currencyconverter.common;

public interface PresenterFactory<View extends BindView, P extends Presenter<View>> {
    P create();
}
