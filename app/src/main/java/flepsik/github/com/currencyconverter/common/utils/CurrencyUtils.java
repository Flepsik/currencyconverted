package flepsik.github.com.currencyconverter.common.utils;

import android.content.ContentValues;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import flepsik.github.com.currencyconverter.model.Currency;
import flepsik.github.com.currencyconverter.repository.database.CurrencyDatabaseHelper;

public class CurrencyUtils {
    public static boolean isValidToSave(Currency currency) {
        return currency != null
                && currency.getId() != 0
                && currency.getNominal() != 0
                && currency.getValue() != 0d
                && !TextUtils.isEmpty(currency.getCode())
                && !TextUtils.isEmpty(currency.getName());
    }

    @NonNull
    public static ContentValues toContentValues(Currency currency) {
        ContentValues values = new ContentValues();
        values.put(CurrencyDatabaseHelper.KEY_ID, currency.getId());
        values.put(CurrencyDatabaseHelper.KEY_CODE, currency.getCode());
        values.put(CurrencyDatabaseHelper.KEY_NAME, currency.getName());
        values.put(CurrencyDatabaseHelper.KEY_NOMINAL, currency.getNominal());
        values.put(CurrencyDatabaseHelper.KEY_VALUE, currency.getValue());
        return values;
    }

    public static List<Currency> parseFeed(InputStream inputStream) throws XmlPullParserException, IOException, ParseException {
        long id = -1;
        String code = null;
        String name = null;
        int nominal = -1;
        double value = -1;
        List<Currency> items = new CopyOnWriteArrayList<>();
        NumberFormat numberFormat = NumberFormat.getInstance();
        try {
            XmlPullParser xmlPullParser = Xml.newPullParser();
            xmlPullParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            xmlPullParser.setInput(inputStream, null);
            xmlPullParser.nextTag();
            while (xmlPullParser.next() != XmlPullParser.END_DOCUMENT) {
                int eventType = xmlPullParser.getEventType();
                String tagName = xmlPullParser.getName();
                if (tagName == null) {
                    continue;
                }

                if (eventType == XmlPullParser.END_TAG) {
                    if (tagName.equalsIgnoreCase("valute")) {
                        if (id != -1 && !TextUtils.isEmpty(code)
                                && !TextUtils.isEmpty(name) && nominal != -1 && value != -1) {
                            Currency item = new Currency(id, code, name, nominal, value);
                            items.add(item);
                            id = -1;
                            code = null;
                            name = null;
                            nominal = -1;
                            value = -1;
                        }
                    }
                    continue;
                }

                if (eventType == XmlPullParser.START_TAG) {
                    if (tagName.equalsIgnoreCase("valute")) {
                        continue;
                    }
                }

                String result = "";
                if (xmlPullParser.next() == XmlPullParser.TEXT) {
                    result = xmlPullParser.getText();
                    xmlPullParser.nextTag();
                }

                if (tagName.equalsIgnoreCase("NumCode")) {
                    id = Long.parseLong(result);
                } else if (tagName.equalsIgnoreCase("CharCode")) {
                    code = result;
                } else if (tagName.equalsIgnoreCase("Nominal")) {
                    nominal = Integer.parseInt(result);
                } else if (tagName.equalsIgnoreCase("Name")) {
                    name = result;
                } else if (tagName.equalsIgnoreCase("value")) {
                    value = numberFormat.parse(result).doubleValue();
                }
            }

            return items;
        } finally {
            inputStream.close();
        }
    }
}
