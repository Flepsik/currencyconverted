package flepsik.github.com.currencyconverter.common;

import android.os.Bundle;
import android.support.annotation.Nullable;

public class PresenterUtils {
    private static final String BUNDLE_TAG = "UniqueTag";
    private static final String START_TAG = "Presenter#";
    private static volatile int sCounter = 0;

    public static String generateUniqueTag() {
        sCounter++;
        return START_TAG + sCounter;
    }

    public static void saveTag(Bundle outState, String tag) {
        outState.putString(BUNDLE_TAG, tag);
    }

    @Nullable
    public static String loadTag(Bundle savedInstance) {
        return savedInstance == null ? null : savedInstance.getString(BUNDLE_TAG);
    }
}