package flepsik.github.com.currencyconverter.common;

import android.os.Bundle;
import android.support.annotation.NonNull;

class PresenterDelegator<View extends BindView> {
    private Presenter<View> presenter;

    PresenterDelegator(Presenter<View> presenter) {
        this.presenter = presenter;
    }

    void onCreate(Bundle savedInstanceState) {
        presenter.onCreate(savedInstanceState);
    }

    void saveState(Bundle outState) {
        presenter.saveState(outState);
    }

    void bindView(@NonNull View view) {
        presenter.bindView(view);
    }

    void unbindView() {
        presenter.unbindView();
    }

    void onDestroy() {
        presenter.onDestroy();
    }
}
