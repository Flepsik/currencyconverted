package flepsik.github.com.currencyconverter.repository.database;

import flepsik.github.com.currencyconverter.model.Currency;

public interface CurrencyRepository extends Repository<Currency> {
    void deleteAll();
}
