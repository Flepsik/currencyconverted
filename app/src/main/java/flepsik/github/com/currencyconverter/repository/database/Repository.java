package flepsik.github.com.currencyconverter.repository.database;

import java.util.List;

import flepsik.github.com.currencyconverter.common.LoadingCallback;

public interface Repository<Item> {
    void insertOrReplace(Item item);

    void insertOrReplace(List<Item> items);

    void delete(long id);

    void findAll(LoadingCallback<List<Item>> callback);

    void find(int index, LoadingCallback<Item> callback);
}
