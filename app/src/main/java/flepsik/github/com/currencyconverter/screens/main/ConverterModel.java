package flepsik.github.com.currencyconverter.screens.main;

import java.util.List;

import flepsik.github.com.currencyconverter.common.LoadingCallback;
import flepsik.github.com.currencyconverter.model.Currency;

public interface ConverterModel {
    void getCurrencies(LoadingCallback<List<Currency>> callback);
}
